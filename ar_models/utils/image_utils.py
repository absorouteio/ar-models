import io


def crop(image, xmin, ymin, xmax, ymax):
    w, h = image.size
    return image.crop((xmin * w, ymin * h, xmax * w, ymax * h))


def image_bytes(image, format='jpeg'):
    img_byte_arr = io.BytesIO()
    image.save(img_byte_arr, format=format)
    return img_byte_arr.getvalue()
