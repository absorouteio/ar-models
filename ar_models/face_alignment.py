import dlib


def get_dlib_rectangle(image, xmin, ymin, xmax, ymax):
    w, h = image.size
    left = int(xmin * w)
    top = int(ymin * h)
    right = int(xmax * w)
    bottom = int(ymax * h)
    return dlib.rectangle(left, top, right, bottom)


class FaceAlignment:

    def __init__(self, model_file, target_size):
        self.predictor = dlib.shape_predictor(model_file)
        self.target_size = int(target_size)

    def align(self, image, rect):
        detection = self.predictor(image, rect)
        return dlib.get_face_chip(image, detection, size=self.target_size)