import time
import tensorflow as tf
import tensorflow.contrib.tensorrt as trt


def load_graph(path_to_ckpt, graph, return_elements=None):
    tf.logging.info('loading frozen graph %s', path_to_ckpt)
    t1 = time.time()
    with graph.as_default():
        graph_def = tf.GraphDef()
        with tf.gfile.GFile(path_to_ckpt, 'rb') as fid:
            serialized_graph = fid.read()
            graph_def.ParseFromString(serialized_graph)
            return_tensors = tf.import_graph_def(graph_def, return_elements=return_elements, name='')
    tf.logging.info('finished loading frozen graph in %d seconds', (time.time()-t1))
    return return_tensors


def get_gpu_config():
    """Share GPU memory between image preprocessing and inference."""
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
    return tf.ConfigProto(gpu_options=gpu_options)


class TfTrtModel:

    out_tensors = ['detection_boxes', 'detection_scores', 'detection_classes']

    def __init__(self, path_to_ckpt):
        graph = tf.Graph()
        return_tensors = load_graph(path_to_ckpt, graph, self.out_tensors)
        with graph.as_default():
            # config = tf.ConfigProto()
            # config.gpu_options.allow_growth = True
            # self.sess = tf.Session(graph=self.graph, config=config)
            self.sess = tf.Session(graph=graph)

        self.pred_tensors = list(map(lambda x: x.outputs[0], return_tensors))

    def run(self, images):
        start_time = time.time()
        (boxes, scores, classes) = self.sess.run(self.pred_tensors, feed_dict={'image_tensor:0': images})
        elapsed_time = time.time() - start_time
        tf.logging.debug('inference (batch size {}) time: {}'.format(len(images), elapsed_time))

        return boxes, scores, classes


class TensorflowModel:

    def __init__(self, path_to_ckpt):
        graph = tf.Graph()
        load_graph(path_to_ckpt, graph)

        with graph.as_default():
            # config = tf.ConfigProto()
            # config.gpu_options.allow_growth = True
            # self.sess = tf.Session(graph=self.graph, config=config)
            self.sess = tf.Session(graph=graph)

        self.image_tensor = graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        boxes = graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        scores = graph.get_tensor_by_name('detection_scores:0')
        classes = graph.get_tensor_by_name('detection_classes:0')

        self.pred_tensors = [boxes, scores, classes]

    def run(self, images):
        start_time = time.time()
        (boxes, scores, classes) = self.sess.run(self.pred_tensors, feed_dict={self.image_tensor: images})
        elapsed_time = time.time() - start_time
        tf.logging.debug('inference (batch size {}) time: {}'.format(len(images), elapsed_time))

        return boxes, scores, classes
