import os
import argparse
import tensorflow as tf
import tensorflow.contrib.tensorrt as trt

tf.logging.set_verbosity(tf.logging.INFO)


def GiB(val):
    return val * 1 << 30


def write_graph_to_file(graph_def, precision, output_dir):
    """write Frozen Graph file to disk."""
    output_path = os.path.join(output_dir, precision.lower())
    output_file = os.path.join(output_path, 'trt_frozen_inference_graph.pb')
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    with tf.gfile.GFile(output_file, "wb") as f:
        f.write(graph_def.SerializeToString())

    tf.logging.info("TF-TRT graph saved to {}".format(output_file))


def convert(args):
    # Inference with TF-TRT frozen graph workflow:
    graph = tf.Graph()
    with graph.as_default():
        # First deserialize your frozen graph:
        with tf.gfile.GFile(args.frozen_graph, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())

        return_tensors_list = args.return_tensors.split(",")
        trt_graph = trt.create_inference_graph(input_graph_def=graph_def,
                                               outputs=return_tensors_list,
                                               max_batch_size=args.max_batch_size * 30,
                                               max_workspace_size_bytes=GiB(args.max_ws_size),
                                               minimum_segment_size=args.min_segment_size,
                                               precision_mode=args.precision)
        # Import the TensorRT graph into a new graph and run:
        return_tensors = tf.import_graph_def(trt_graph, return_elements=return_tensors_list, name='')
        tf.logging.info('%d TRTEngineOp converted', len([1 for n in trt_graph.node if str(n.op) == 'TRTEngineOp']))

        write_graph_to_file(trt_graph, args.precision, args.out_dir)


def build_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--frozen_graph",
                        help="path to frozen graph.", required=True, type=str)
    parser.add_argument("-r", "--return_tensors",
                        help="comma-separated string of output tensor names", required=True, type=str)
    parser.add_argument("-b", "--max_batch_size",
                        help="max batch size", required=True, type=int)
    parser.add_argument("-ws", "--max_ws_size",
                        help="max workspace size in gigabytes", required=True, type=int)
    parser.add_argument("-p", "--precision",
                        help="precision either fp16 or fp32", required=True, type=str)
    parser.add_argument("-o", "--out_dir",
                        help="output directory of tf-trt frozen graph", required=True, type=str)
    parser.add_argument("-m", "--min_segment_size",
                        help="minimum segment size", required=False, default=3, type=int)


    return parser


if __name__ == '__main__':
    args = build_argparser().parse_args()

    convert(args)