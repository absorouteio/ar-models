import cv2
import base64
import argparse
import threading
import time
import queue
import numpy as np
from PIL import Image
import tensorflow as tf
import tensorflow.contrib.tensorrt as trt

tf.logging.set_verbosity(tf.logging.DEBUG)

from ar_models.utils import image_utils
from ar_models.face_alignment import get_dlib_rectangle, FaceAlignment
from ar_models.utils import visualization_utils as vis_utils
from ar_models.model import TfTrtModel, TensorflowModel

BUFFER_SIZE = -1
BATCH_SIZE = 8

input_q = queue.Queue(BUFFER_SIZE)  # a queue of input video frames
input_condition = threading.Condition()  # a condition object to notify other threads of video frame availability
inference_q = queue.Queue(BUFFER_SIZE)  # a queue of inference results
inference_condition = threading.Condition()  # a condition object to notify other threads of inference results availability


def _push_and_notify(condition, q, data):
    with condition:
        q.put(data)
        condition.notifyAll()


def _postprocess(boxes, scores, classes, threshold):
    results = []
    for img_boxes, img_scores, img_classes in zip(boxes, scores, classes):

        items = []
        for (box, score, class_) in zip(img_boxes, img_scores, img_classes):
            if score > threshold:
                xmin, ymin, xmax, ymax = float(box[1]), float(box[0]), float(box[3]), float(box[2])
                item = {
                    'class': 'face' if class_ == 1. else 'person',
                    'score': score,
                    'box': {
                        'xmin': xmin,
                        'ymin': ymin,
                        'xmax': xmax,
                        'ymax': ymax
                    }
                }
                items.append(item)
        results.append(items)

    return results


def _inference(model, inputs, width=300, height=300, threshold=0.5):
    resized = [cv2.resize(inp, (width, height)) for inp in inputs]
    boxes, scores, classes = model.run(resized)
    return _postprocess(boxes, scores, classes, threshold)


def _align_faces(faces, images, model):
    # TODO: seems like alignment can be performed multiple images at once. Improve later
    for img_faces, img in zip(faces, images):
        for face in img_faces:
            box = face['box']
            pil_img = Image.fromarray(img)
            rect = get_dlib_rectangle(pil_img, box['xmin'], box['ymin'], box['xmax'], box['ymax'])
            aligned = Image.fromarray(model.align(img, rect))
            face['face_img'] = base64.b64encode(image_utils.image_bytes(aligned)).decode()


def _write(path):
    import skvideo.io
    writer = skvideo.io.FFmpegWriter("out.mp4")
    cap = cv2.VideoCapture(path)

    while not inference_q.empty():
        batch_preds = inference_q.get()
        # print(len(batch_preds), batch_preds)
        for preds in batch_preds:
            ret, image = cap.read()
            for pred in preds:
                box = pred['box']
                class_ = pred['class']
                color = 'Chartreuse' if pred['class'] == 'face' else 'AliceBlue'
                vis_utils.draw_bounding_box_on_image_array(image,
                                                           box['ymin'],
                                                           box['xmin'],
                                                           box['ymax'],
                                                           box['xmax'],
                                                           color=color,
                                                           thickness=5,
                                                           display_str_list=[class_],
                                                           use_normalized_coordinates=True)
                writer.writeFrame(image)

    cap.release()
    writer.close()
    cv2.destroyAllWindows()


class StreamWorker(threading.Thread):
    """reads video frame from video input. The source can be stream of video from camera."""

    def __init__(self, path):
        super().__init__()
        self.path = path

    def run(self):
        tf.logging.info('starting StreamWorker...')
        cap = cv2.VideoCapture(self.path)

        frame_count = 0
        batch = []
        while True:
            ret, image = cap.read()

            if ret:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                batch.append(image)

                # only notify InferenceWorker when the number video frames is equal to the specified batch size
                if len(batch) % BATCH_SIZE == 0:

                    _push_and_notify(input_condition, input_q, batch)

                    tf.logging.debug('producer batch size %d', len(batch))
                    batch = []

                frame_count += 1
            else:  # end of video stream, terminate the loop. This is not required when using real camera.
                _push_and_notify(input_condition, input_q, batch)
                break

        tf.logging.debug('frame count %d', frame_count)
        cap.release()
        cv2.destroyAllWindows()


class InferenceWorker(threading.Thread):

    def __init__(self, file, landmarks_det_file, input_video, align_target_size=160):
        super().__init__()
        # self.person_detection = TensorflowModel(person_detection_ckpt)
        # self.face_detection = TensorflowModel(face_detection_ckpt)
        tf.logging.debug('loading frozen graph %s', file)
        t1 = time.time()
        self.face_person_det = TfTrtModel(file)
        tf.logging.debug('finished loading frozen graph in %d seconds', (time.time() - t1))
        self.face_align = FaceAlignment(landmarks_det_file, align_target_size)
        self.input_video = input_video

    def run(self):
        tf.logging.info('starting InferenceWorker...')
        times_elapsed = []
        frames_count = 0
        while True:
            with input_condition:

                while input_q.empty():
                    input_condition.wait()
                images = input_q.get()
                if len(images) < BATCH_SIZE:
                    tf.logging.info(times_elapsed)
                    tf.logging.info('fps %f', (frames_count-1) / np.sum(times_elapsed[1:]))
                    break
                frames_count += len(images)

            if images:
                t1 = time.time()
                tf.logging.debug('inference batch size %d', len(images))
                # person_pred = _inference('person', self.person_detection, images, threshold=.5)
                # face_pred = _inference('face', self.face_detection, images, threshold=.7)
                preds = _inference(self.face_person_det, images, threshold=0.5)
                _align_faces(preds, images, self.face_align)
                time_elapsed = time.time() - t1

                # Since GPU is available, batch prediction gives better performance than that of the non-batch
                # prediction. The predictions are of the following format,
                # [[frame1_pred], [frame2_pred], ..., [framen_pred]]
                # results = [a + b for a, b in zip(person_pred, face_pred)]
                results = preds
                _push_and_notify(inference_condition, inference_q, results)

                times_elapsed.append(time_elapsed)

            # end of video stream, terminate the loop. This is not required when using real camera.
            if len(images) < BATCH_SIZE:
                # _write(self.input_video)
                tf.logging.debug('fps %f', frames_count / np.sum(times_elapsed))
                break


class OutputWorker(threading.Thread):

    def __init__(self):
        super().__init__()

    def run(self):
        tf.logging.info('starting OutputWorker...')
        while True:
            with inference_condition:

                while inference_q.empty():
                    inference_condition.wait()
                results = inference_q.get()
                tf.logging.debug('inference results (size %d): %s', len(results), results)

            # end of video stream, terminate the loop. This is not required when using real camera.
            if len(results) < BATCH_SIZE:
                break


def build_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input",
                        help="Path to video file or image.", required=True, type=str)
    parser.add_argument("-m", "--model",
                        help="Path to face person detection model ckpt file", required=True, type=str)
    # parser.add_argument("-pdm", "--person_detection_model",
    #                     help="Path to person detection model ckpt file", required=True, type=str)
    # parser.add_argument("-fdm", "--face_detection_model",
    #                     help="Path to face detection model ckpt file", required=True, type=str)
    parser.add_argument("-fam", "--face_alignment_model",
                        help="Path to face alignment model file", required=True, type=str)

    return parser


if __name__ == '__main__':
    args = build_argparser().parse_args()

    c = InferenceWorker(file=args.model,
                        landmarks_det_file=args.face_alignment_model,
                        input_video=args.input)
    c.start()

    p = StreamWorker(path=args.input)
    p.start()

    o = OutputWorker()
    o.start()
