from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'tensorflow-gpu==1.13.1+nv19.4',
    'dlib==19.17.0',
    'Pillow==6.0.0'
]

DEPENDENCY_LINKS=[
    'https://developer.download.nvidia.com/compute/redist/jp/v42'
]


setup(name='ar-models',
      version='0.0.2',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/absorouteio/ar-models',
      include_package_data=True,
      description='absoroute.io models and utils',
      author='Peeranat Fupongsiripan',
      author_email='peeranat@absoroute.io',
      install_requires=REQUIRED_PACKAGES,
      dependency_links=DEPENDENCY_LINKS,
      zip_safe=False)